<?php
namespace Drupal\online_shop\Controller;
use Drupal\Core\Controller\ControllerBase;

class OnlineShopController extends ControllerBase {
  /**
   * Test page.
   */
  public function online_shop_hello() {
    //$result = views_embed_view('content', 'page_1');
    $page_san_pham_get = views_embed_view('page_san_pham', 'page_san_pham');
    $page_san_pham= drupal_render($page_san_pham_get);

    //Danhsach
    $page_danhsach_tho= \Drupal\online_shop\Controller\OnlineShopController::online_shop_danhsach();
    $page_danhsach= drupal_render($page_danhsach_tho);

    //Form



    $all_page = <<<HTML
    <html>
    <body><h1>{$page_danhsach}</h1>
    <div class='form-contact'>{{ form.form_build_id }}</div>
    <div class='my-example-class'>{$page_san_pham}</div>

    </body>
    </html>
    HTML;
    // return array('#markup' => drupal_render($result));
    // return array('#markup' => t('hello you'));
    return array('#markup' => $all_page);
  }

  public function online_shop_danhsach(){
      $danhsach = array(
        array('name' => 'DS 1'),
        array('name' => 'DS 2'),
        array('name' => 'DS 3'),
        array('name' => 'DS 4'),
        array('name' => 'DS 5'),
      );

      return array(
        //chi-tiet.html.twig
        '#theme'=> 'danh_sach',
        '#danhsach' => $danhsach,
        '#tieude' => 'TIEU DE Controler php'
      );
  }

}

