<?php
namespace Drupal\online_shop\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

//use Drupal\Core\Url;
//use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Implements a simple form.
 */
class OnlineShopContactForm extends FormBase {

  /**
   * Build the simple form.
   *
   * @param array $form
   *   Default form array structure.
   * @param FormStateInterface $form_state
   *   Object containing current form state.
   *
   * @return $form
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Your name'),
      '#required' => true,
      '#weight' => 1,
    );

    $form['phone'] = array(
      '#type' => 'tel',
      '#title' => t('Your phone number'),
      '#weight' => 2,
    );

    $form['message'] = array(
      '#type' => 'textarea',
      '#title' => t('Your message'),
      '#required' => true,
      '#weight' => 3,
    );

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Send'),
      '#weight' => 4,
    );


    return $form;
  }

  /**
   * Getter method for Form ID.
   *
   * The form ID is used in implementations of hook_form_alter() to allow other
   * modules to alter the render array built by this form controller.  it must
   * be unique site wide. It normally starts with the providing module's name.
   *
   * @return string
   *   The unique ID of the form defined by this class.
   */
  public function getFormId() {
    return 'online_shop_contact_form';
  }

  /**
   * Implements form validation.
   *
   * The validateForm method is the default method called to validate input on
   * a form.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $phone = $form_state->getValue('phone');
    if (!is_numeric($phone) or strlen($phone) != 10) {
      $form_state->setErrorByName('phone', $this->t('Your phone number is invalid.'));
    }
  }

  /**
   * Implements a form submit handler.
   *
   * The submitForm method is the default method called for any submit elements.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Set successful message.
    drupal_set_message(t('Thank you for contacting us.'));

    // Redirect to front page.
    $form_state->setRedirect('<front>');
  }
}